# YouTuber
A Laravel 5 package that gathers intelligence about YouTube videos and playlists.

## Installation
Require the package in your application using the following command:
```
composer require hermes/youtuber
```

## Usage



## Credits

- [PHP YouTube API by MadCoda](https://github.com/madcoda/php-youtube-api)
- [YouTube Downloader by Athlon1600](https://github.com/Athlon1600/youtube-downloader)

