<?php

namespace Hermes\Youtuber\Facades;

use Illuminate\Support\Facades\Facade;

class YoutuberFacade extends Facade
{
	protected static function getFacadeAccessor()
	{
		return 'youtuber';
    }
}
