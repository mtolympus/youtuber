<?php

namespace Hermes\Youtuber\Providers;

use Hermes\Youtuber\Youtuber;
use Illuminate\Support\ServiceProvider;

class YoutuberServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootstrapConfiguration();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('youtuber', function() {
            return new Youtuber;
        });
    }

    /**
     * Bootstrap configuration
     * Sets up merging & publishing of the config file
     * 
     * @return      void
     */
    private function bootstrapConfiguration()
    {
        // Setup merging of the config file 
        $this->mergeConfigFrom(__DIR__."/../Config/config.php", "youtuber");

        // Setup publishing of the config file
        $this->publishes([__DIR__."/../Config/config.php" => config_path("youtuber.php")]);
    }
}