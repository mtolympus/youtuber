<?php

namespace Hermes\Youtuber;

use Exception;
use YouTubeDownloader;
use Madcoda\Youtube\Youtube;

/**
 * This is the YouTuber class responsible for gathering intelligence
 * regarding YouTube videos and playlists
 * 
 * @author      Nick Verheijen <verheijen.webdevelopment@gmail.com>
 */
class Youtuber
{
    private $youtubeApi;
    private $youtubeDownloader;

    public function __construct()
    {
        $this->youtubeApi = new Youtube(["key" => "AIzaSyCpjRy3-JahSFKpROomHc3r5dLX7hgjBE8"]);
        $this->youtubeDownloader = new YouTubeDownloader();
    }

    //
    // Videos
    //

    /**
     * Determine the type of a YouTube video link
     *
     * @param       string              The link to the video
     * @return      string              Returns: full, short or false for invalid
     */
    public function determineLinkType($link)
    {
        // Full link: https://www.youtube.com/watch?v=xxxxxxxxx
        if (preg_match("/^https:\/\/www\.youtube\.com\/watch\?v=.*/", $link)) 
        {
            return "full";
        }
        // Short link: https://youtu.be/xxxxxxx
        else if (preg_match("/^https:\/\/youtu\.be\/.*/", $link))
        {
            return "short";
        }

        // Did not match any patterns so it can't be a YouTube link
        return false;
    }

    /**
     * Extract the video's ID from the link
     *
     * @param       string              The link to the video
     * @return      string              The code representing the video
     */
    public function extractVideoId($link)
    {
        switch ($this->determineLinkType($link))
        {
            case "short":
                return explode("https://youtu.be/", $link)[1];

            case "full":
                $parts = explode("v=", $link);
                return $parts[1];
        }

        return false;
    }

    /**
     * Gather the video's information using the code (extracted from the link)
     *
     * @param       string              The link to the video
     * @return      object              Containing all of the video's information
     */
    public function getVideoInfoByLink($videoLink)
    {
        $videoId = $this->extractVideoId($videoLink);

        if ($videoId)
        {
            return $this->getVideoInfoById($videoId);
        }

        return false;
    }

    /**
     * Gather the video's information using the code (extracted from the link)
     *
     * @param       string              The ID (or code) of the video
     * @return      object,bool         Info object or false if video code was invalid
     */
    public function getVideoInfoById($videoId)
    {
        return $this->youtubeApi->getVideoInfo($videoId);
    }

    //
    // Playlists
    //
    
    /**
     * Determine if a playlist link is valid
     * 
     * @param       String              $link we want to check
     * @return      Boolean
     */
    public function playlistLinkIsValid($link)
    {
        $parts = parse_url($link);
        parse_str($parts["query"], $params);
        return array_key_exists("list", $params);
    }

    /**
     * Extract the playlist's ID from the link
     *
     * @param       string              The link to the playlist
     * @return      string              The code representing the playlist
     */
    public function extractPlaylistId($link)
    {
        $parts = parse_url($link);
        parse_str($parts["query"], $params);
        if (array_key_exists("list", $params))
        {
            return $params["list"];
        }
        return false;
    }

    /**
     * Get playlist information by the playlist link
     * 
     * @param       string              $link to the playlist
     * @return      object              $object representing the playlist
     */
    public function getPlaylistInfoByLink($link)
    {
        $id = $this->extractPlaylistId($link);
        
        return $this->getPlaylistInfoById($id);
    }
    
    /**
     * Get playlist information by the playlist id
     * 
     * @param       string              $id of the playlist
     * @return      object              $object representing the playlist
     */
    public function getPlaylistInfoById($channelId)
    {
        return (object) [
            "playlist" => $this->youtubeApi->getPlaylistById($channelId),
            "videos"   => $this->youtubeApi->getPlaylistItemsByPlaylistId($channelId)
        ];
    }
    
    /**
     * Gather the video's source links (direct links to the actual video) for all qualities
     *
     * @return      Array or false 
     */
    public function getVideoDownloadLinks($link)
    {
        $links = $this->youtubeDownloader->getDownloadLinks($link);
        if (count($links) > 0)
        {
            $supported_formats = [
                "MP4 720p (HD)",
                "WebM 360p",
                "MP4 360p",
                "3GP 144p",
            ];

            $out = [];

            foreach ($links as $link)
            {
                if (in_array($link["format"], $supported_formats))
                {
                    $out[] = $link;
                }
            }

            return $out;
        }

        return false;
    }
    
    //
    // Channels
    //
    
    public function channelLinkIsValid(string $link)
    {
        // https://www.youtube.com/channel/UCn-ibDkEUZnToED36Azm1Xw
        return preg_match("/^https:\/\/www\.youtube\.com\/channel\/.*/", $link);
    }

    /**
     * Extract channel's id from it's link
     * 
     * @param       String                  $link to the channel
     * @return      String                  The channel's ID
     */
    public function extractChannelId(string $link)
    {
        return explode("https://www.youtube.com/channel/", $link)[1];
    }

    /**
     * Get a channel by it's name
     * 
     * @param       string              $name of the channel
     * @return      object              Object representing the channel
     */
    public function getChannelByName(string $channelName)
    {
        return $this->youtubeApi->getChannelByName($channelName);
    }

    /**
     * Get channel by link
     * 
     * @param       String                  $link to the channel
     * @return      
     */
    public function getChannelByLink(string $link)
    {
        $channelId = $this->extractChannelId($link);
        return $this->getChannelById($channelId);
    }

    /**
     * Get a channel by it's ID
     * 
     * @param       string              $id of the channel
     * @return      object              Object representing the channel
     */
    public function getChannelById(string $channelId)
    {
        $channel = $this->youtubeApi->getChannelById($channelId);
        if ($channel)
        {
            $playlists = $this->youtubeApi->getPlaylistsByChannelId($channelId);
            if (count($playlists) > 0)
            {
                for ($i = 0; $i < count($playlists); $i++)
                {
                    $playlists[$i]->videos = $this->youtubeApi->getPlaylistItemsByPlaylistId($playlists[$i]->id);
                }
            }   
            $channel->playlists = $playlists;

            return $channel;
        }

        return false;
    }

    /**
     * Get channel activities
     * 
     * @param       string              $channel id
     * @return      object              
     */
    public function getChannelActivitiesById($channelId)
    {
        return $this->youtubeApi->getActivitiesByChannelId($channelId);
    }

    /**
     * Get playlists
     * 
     * @param       string              $channel id
     * @return      array               Array of objects representing the playlists
     */
    public function getChannelPlaylistsById(string $channelId)
    {
        return $this->youtubeApi->getPlaylistsByChannelId($channelId);
    }

    /**
     * Search YouTube for channels, playlists and videos
     * 
     * @param       string              $searchQuery
     * @return      array               array of objects representing channels, playlists and videos
     */
    public function searchYoutube($query)
    {
        return $this->youtubeApi->search($query);
    }

    /**
     * Search YouTube for videos
     * 
     * @param       string              $query to search for
     * @return      array               array of objects representing videos
     */
    public function searchYoutubeForVideos($query)
    {
        return $this->youtubeApi->searchVideos($query);
    }

    /**
     * Search YouTube channel for videos
     * 
     * @param       string              $query to search for
     */
    public function searchYoutubeChannelForVideos($query)
    {
        return $this->youtubeApi->searchChannelVideos($query);
    }

    /**
     * Convert the duration we get from the Youtuber package
     *
     * @param       String              1H20M38S format
     * @return      String              00:00:00 format
     */
    public function formatDuration($duration)
    {
        $hours = 0;
        $minutes = 0;
        $seconds = 0;

        $duration = str_replace("PT", "", $duration);
        $exploded_by_hours = explode("H", $duration);
        if (count($exploded_by_hours) > 1) {
            $hours = intval($exploded_by_hours[0]);
            $duration = $exploded_by_hours[1];
        }
        $exploded_by_minutes = explode("M", $duration);
        if (count($exploded_by_minutes) > 1) {
            $minutes = intval($exploded_by_minutes[0]);
            $duration = $exploded_by_minutes[1];
        }
        $exploded_by_seconds = explode("S", $duration);
        if (count($exploded_by_seconds) > 1) {
            $seconds = intval($exploded_by_seconds[0]);
        }

        return ( $hours < 10 ? "0" . $hours : $hours ).":".( $minutes < 10 ? "0".$minutes : $minutes ).":".( $seconds < 10 ? "0".$seconds : $seconds );
    }
}